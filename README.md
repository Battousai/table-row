# Table Row

Just a row that uses display table.

## Installing with bower:

Run `bower i --save position-middle=git@bitbucket.org:Battousai/table-row.git#~1.0.0`
in your terminal

### Bootstrap

If you want to use bootstrap's column sizes, remove its float and padding.
